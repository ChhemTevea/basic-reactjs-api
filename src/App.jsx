import React from 'react'
import Home from "./routes/Home";
import { BrowserRouter, Navigate, Route,Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import FPage from './components/home/home';
import Layout from './components/layout';
import ProductDetail from './components/product/ProductDetails';
import Login from "./modules/auth/components/Login";
import {useAuth} from "./modules/auth/components/Auth";
const App = () => {
  const {auth} = useAuth();

  return (

     <BrowserRouter>
      <Routes>
      {auth ? (
        <Route path="/" element={<Layout />}>
        <Route index element={<Home />} />
        <Route path="product/:id" element={<ProductDetail />} />
        <Route path="*" element={<Navigate to="/" />}></Route>
      </Route>


      ) : (
        <>
          <Route index path="auth/login" element={<Login />} />
          <Route path="*" element={<Navigate to="/auth/login" />} />
        </>
      )}
    </Routes>
    </BrowserRouter>
  
  )
}

export default App