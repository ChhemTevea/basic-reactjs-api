import "./ProductStyle.css";

const  Product= ({thumbnail,title,price}) => {
  return <div className="product d-flex flex-column gap-2 pt-3">
    <div className="imgContainer">
    <img src={thumbnail}  alt="Product thumbnail" />
    </div>
    <h2 className="title-name">{title}</h2>
    <p className="text-primary fw-bold">${price}</p>
  </div>; 
};

export default Product;
