import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {getProductById} from "../core/request";
import "./ProductDetailsStyle.css"
import { Link } from "react-router-dom";


const ProductDetail = () => {
    const {id} = useParams();
    const [product, setProduct] = useState({});


    useEffect(() => {
        getProductById(id).then((response)=>{
            setProduct(response.data);
        })
    }, [id]);

    if (!product.id){
        return  <div className="text-center fs-6">
            Loading...
        </div>
    }

    const imgs = document.querySelectorAll('.img-select a');
    const imgBtns = [...imgs];
    let imgId = 1;

    imgBtns.forEach((imgItem) => {
        imgItem.addEventListener('click', (event) => {
            event.preventDefault();
            imgId = imgItem.dataset.id;
            slideImage();
        });
    });

function slideImage(){
    const displayWidth = document.querySelector('.img-showcase img:first-child').clientWidth;

    document.querySelector('.img-showcase').style.transform = `translateX(${- (imgId - 1) * displayWidth}px)`;
}

window.addEventListener('resize', slideImage);

    return <>
        <div className="container-fluid ">
        <Link className="btn btn-primary mt-5 ms-5" to="../" relative="path"> Back </Link>
        <div className="card-wrapper">
        <div className="card">
        <div class = "product-imgs">
        <div class = "img-display">
            <div class = "img-showcase">
                {product.images.map((val,ind)=>{
                    return <img src = {val} alt = "shoe image" style={{height: "550px",width: "150px"}} />
                })}
   
            </div>
          </div>
          <div class = "img-select">
            {product.images.map((val,ind)=>{
                    return <div class = "img-item">
                    <a href = "#" data-id = {ind+1}>
                      <img className="rounded" src = {val} alt = "shoe image" style={{height: "150px",width: "150px"}} />
                    </a>
                  </div>
            })}

          </div>
        </div>

        <div class = "product-content">
          <h2 class = "product-title">{product.title}   </h2>
         
          <div class = "product-price">
            <p class = "new-price">Discount : <span>{product.discountPercentage} %</span></p>
            <p class = "new-price">New Price: <span>${product.price}</span></p>
          </div>

          <div class = "product-detail">
            <h2>about this item: </h2>
            <p>{product.description}</p>
            <ul>
              <li>Brand: <span>{product.brand}</span></li>
              <li>Available: <span>in stock ({product.stock}) </span></li>
              <li>Category: <span>{product.category}</span></li>
              <li>Shipping Area: <span>All over the world</span></li>
              <li>Shipping Fee: <span>Free</span></li>
            </ul>
          </div>

          <div className="purchase-info d-flex flex-wrap"><input type="number" min="1" defaultValue="1" style={{marginBottom: "15px"}} />
            <button type = "button" class = "btn">
              Add to Cart <i class = "fas fa-shopping-cart"></i>
            </button>
            <button type = "button" class = "btn">Compare</button></div>
        </div>
        </div>
    </div>
        </div>
    </>
}
export default ProductDetail;