
import { Button } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { useAuth } from '../../modules/auth/components/Auth';

function NavScrollExample() {
    
  const {logout} = useAuth();
  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Container fluid className="px-5">
        <Navbar.Brand className='fw-bold fs-3'>Kilo APP</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
            <Nav.Link to="/">Home</Nav.Link>
            <Nav.Link href="#action2">About Us</Nav.Link>
            <Nav.Link href="#action2">Product</Nav.Link>
            <Nav.Link href="#action2">Contact</Nav.Link>
            
          </Nav>
        </Navbar.Collapse>
        <Button variant='warning' onClick={logout}>
                    Logout
                </Button>

      </Container>
    </Navbar>
  );
}

export default NavScrollExample;