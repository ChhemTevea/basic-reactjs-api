import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { AuthProvider } from "./modules/auth/components/Auth";
import { AuthInit } from "./modules/auth/components/AuthInit";
import { setUpAxios } from './modules/auth/components/AuthHelper';
const root = ReactDOM.createRoot(document.getElementById('root'));
setUpAxios();
root.render(
    <React.StrictMode>
        <AuthProvider>
            <AuthInit>
                <App />
            </AuthInit>
        </AuthProvider>
    </React.StrictMode>
);
reportWebVitals();