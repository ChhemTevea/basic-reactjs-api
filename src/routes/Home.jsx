import { useEffect, useState } from "react";
import { getCategories, getListProduct, getListProductByCategory, searchProduct } from "../components/core/request";
import { useNavigate } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Badge from 'react-bootstrap/Badge';
import PaginationBasic from "../components/product/Pagination";
import Product from "../components/product/Product";

const Api = () => {
  const [products, setProducts] = useState([]);
    const [active, setActive] = useState("All");
    const [categories, setCategories] = useState([]);
    const [searchText, setSearchText] = useState("");
    const [total, setTotal] = useState(0);
    const navigate = useNavigate();

    const limit = 8;

    useEffect(() => {
        getListProduct(limit).then((response) => {
            setProducts(response.data.products);
            setTotal(response.data.total);
        });

        getCategories().then((response) => {
            setCategories(response.data);
        })
    }, []);


    useEffect(() => {
        active === "All" ?
            getListProduct(limit).then((response) => {
                setProducts(response.data.products);
                setTotal(response.data.total);
            })
            : getListProductByCategory(limit, active).then((response) => {
                setProducts(response.data.products);
                setTotal(response.data.total);

            });
    }, [active]);


    const onSearch = () => {
        searchProduct({ q: searchText }).then((response) => {
            setProducts(response.data.products);
        });
    }



    return <div className="container mt-5">
        <div className="w-100 d-flex justify-content-center mb-4">
            <Form className="d-flex justify-content-center px-5 w-50">
                <Form.Control
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"
                    onChange={(e) => setSearchText(e.target.value)}
                />
                <Button onClick={onSearch} variant="outline-success">Search</Button>
            </Form>
        </div>

        <div className="container-fluid">
        <div className="px-4">
            <div className="d-flex overflow-x-scroll w-100 pb-3">
                <Badge onClick={() => setActive("All")} className="px-4 py-2 fs-6 mx-2" bg={active === "All" ? "primary" : "secondary"}>All</Badge>
                {categories.map((item, index) =>
                    <Badge onClick={() => setActive(item)} key={index} className="px-4 py-2 fs-6 mx-2 " bg={active === item ? "primary" : "secondary"}>{item}</Badge>)}
            </div>
        </div>
        </div>


        <div className="container-fluid">
        <div className="row d-flex flex-wrap">
            {products.length === 0 && <div>
                Not Found
            </div>}
            {products.map((product, index) => {
                return <div onClick={() => navigate(`product/${product.id}`)} className="col-md-3 p-3" key={index}>
                    <Product thumbnail={product.thumbnail} title={product.title} price={product.price}/>
                </div>
            })}
        </div>
        </div>
        <div className="d-flex justify-content-center my-3">
            <PaginationBasic onChangePage={(page) => {
                getListProduct(limit, (page - 1) * limit).then((response) => {
                    setProducts(response.data.products);
                    setTotal(response.data.total);
                });

            }} total={total / limit} />
        </div>
    </div>
}

export default Api;
