import { useState } from "react";
import { useAuth } from "./Auth";
import { login } from "../core/request";
import "./LoginStyle.css";

const Login = () => {

    const [username, setUsername] = useState("kminchelle");
    const [pass, setPass] = useState("0lelplR");
    const [error, setError] = useState("");
    const { saveAuth } = useAuth();

    const onLogin = (event) => {
        event.preventDefault();
        login(username, pass).then((response) => {
            setError("");
            saveAuth(response.data.token)   
        }).catch((error) => {
            setError(error.response.data.message);
        })
    }

    return <div className="container-fluid">
        <form className="w-25 mx-auto p-5 mt-5 shadow-sm rounded login-form" style={{border: "1.5px solid #ffc107"}}>
        <h1 className="text-center text-dark mt-5 fw-1 text-uppercase">Login Form</h1>
        <div className="text-center">
            <img src="https://static.vecteezy.com/system/resources/previews/008/325/229/non_2x/kali-linux-dragon-free-vector.jpg"
                className="mx-auto profile-image-pic img-thumbnail rounded-circle my-3" alt="profile" style={{width:"100px"}} />
        </div>

        <div className="mb-3">
            <input type="text" className="form-control" id="Username" aria-describedby="emailHelp"
                placeholder="User Name" onChange={(e) => setUsername(e.target.value)} />
        </div>
        <div className="mb-3">
            <input type="password" className="form-control" id="password" placeholder="password"
                onChange={(e) => setPass(e.target.value)} />
        </div>
        <div className="text-center">
            <button onClick={onLogin} className="btn btn-warning px-5 mb-5 w-100">Login</button>
        </div>
        <div className="text-center text-danger">{error}</div>

    </form>
    </div>
}


export default Login;
